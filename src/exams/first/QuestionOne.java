package exams.first;

import java.util.Scanner;

public class QuestionOne {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in); //add scanner
		
		System.out.print("Enter number: "); //prompt for input
		int i = input.nextInt(); //store input as int
		
		char c = (char) ((i)+65); //math function and store as char
		
		System.out.print(c); //print out char

		input.close(); //close scanner
	}

}
