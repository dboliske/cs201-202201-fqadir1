package exams.first;

import java.util.Scanner;

public class QuestionTwo {

	public static void main(String[] args) {
		
Scanner input = new Scanner(System.in); //add scanner
		
		System.out.print("Enter number: "); //prompt for input
		int i = input.nextInt(); //store input as int
		
		if (i%2 == 0)
		{System.out.println("foo");} //checks for divisible by 2
			
		if (i%3 == 0)
		{System.out.println("bar");} //checks for divisible by 3
		
		else if ((i%2 == 0 )||(i%3 == 0))
		{System.out.println("foobar");} //checks for divisible by both
		
		
	input.close();
	}

}
