# Midterm Exam

## Total

54/100

## Break Down

1. Data Types:                  18/20
    - Compiles:                 5/5
    - Input:                    3/5
    - Data Types:               5/5
    - Results:                  5/5
2. Selection:                   18/20
    - Compiles:                 5/5
    - Selection Structure:      8/10
    - Results:                  5/5
3. Repetition:                  18/20
    - Compiles:                 5/5
    - Repetition Structure:     5/5
    - Returns:                  5/5
    - Formatting:               3/5
4. Arrays:                      0/20
    - Compiles:                 -/5
    - Array:                    -/5
    - Exit:                     -/5
    - Results:                  -/5
5. Objects:                     0/20
    - Variables:                -/5
    - Constructors:             -/5
    - Accessors and Mutators:   -/5
    - toString and equals:      -/5

## Comments

1. Good, but doesn't confirm that the input is an integer.
2. Good, but doesn't confirm that the input is an integer.
3. Good, but doesn't confirm that the input is an integer and the formatting is off.
4. Not submitted
5. Not submitted
