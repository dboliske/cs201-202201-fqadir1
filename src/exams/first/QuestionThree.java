package exams.first;

import java.util.Scanner;

public class QuestionThree {

	public static void main(String[] args) {
		
		Scanner input = new Scanner (System.in);
		
		System.out.println("Enter number for dimensions: "); //prompts for dimension
		
		int dim = input.nextInt();
			
		for (int i= dim-1; i>=0 ; i--) //outer for dimension
		{for (int j=0; j<=i; j++) //inner loop for blank spaces
		{System.out.print("*" + "");} //prints asterisk and space
		
		System.out.println(); //moves to next line
		}
		
		input.close();
		
	}

}
