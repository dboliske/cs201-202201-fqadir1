package labs.lab1;
import java.util.Scanner;
public class Exercise4 {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);

        System.out.print("Input a degree in Fahrenheit: ");
        double Fahrenheit = in.nextDouble();

        double  celsius =(( 5 *(Fahrenheit - 32.0)) / 9.0);
        System.out.println(Fahrenheit + " degree Fahrenheit is equal to " + celsius + " in Celsius");
		
        System.out.print("Input a degree in Celcius: ");
        double Celcius = in.nextDouble();

        double  Farenheit =(Celcius*(9/5) + 32);
        System.out.println(Celcius + " degree Celcius is equal to " + Farenheit + " in Farenheit");
		
	}

}
