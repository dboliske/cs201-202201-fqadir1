# Lab 1

## Total

15.5/20

## Break Down

* Exercise 1    2/2
* Exercise 2    1.5/2
* Exercise 3    2/2
* Exercise 4
  * Program     2/2
  * Test Plan   0/1
* Exercise 5
  * Program     1/2
  * Test Plan   0/1
* Exercise 6
  * Program     2/2
  * Test Plan   0/1
* Documentation 5/5

## Comments
2. Height in inches is only effectively multiplied by 2, not 2.54
4. No test plan/table
5. No test plan/table. Have to convert from inches input to sq. feet output. Also have to calculate the surface area, not volume.
6. No test plan/table.