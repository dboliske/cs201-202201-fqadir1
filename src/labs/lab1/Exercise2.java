package labs.lab1;

public class Exercise2 {

	public static void main(String[] args) {
		
		System.out.println(Math.subtractExact(66, 28)); //age subtracted by fathers age
		
		System.out.println(Math.multiplyExact(1993, 2)); //birth year multiplied
		
		int heightinches = 75;
		
		System.out.println(Math.multiplyExact(heightinches, (int) 2.54)); // convert height to cm
		
		System.out.println((Math.floorDiv(heightinches, 12)) + " " + "feet and" + " " + (heightinches % 12) + " " + "inches"); //convert height from inches to feet and inches
		
		
	}

}
